//
//  ViewController.m
//  newmotion
//
//  Created by Adrian Ortuzar on 10/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDataManager.h"
#import "UserViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonAction:(id)sender {
    NSString *user = self.userTextField.text;
    NSString *pass = self.passwordTextField.text;
    
    if ([self loginValidationWithUser:user andPassword:pass]) {
        
        [self.view endEditing:YES];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [[AppDataManager sharedManager] loginWithName:@"name" andPassword:@"pass" withCompletionHandler:^(NSDictionary *user, NSError *error){
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error == nil) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *userVc = [storyboard instantiateViewControllerWithIdentifier:@"UserVC"];
                [self.navigationController pushViewController:userVc animated:YES];
            }else{
                [self displayUIAlertViewWith:@"Login Error" andMessage:@"Right now we can not log in, try it later. Thank you."];
            }
        }];
    }else{
        [self displayUIAlertViewWith:@"Login Error" andMessage:@"User and Password can not be empty"];
    }
}

- (BOOL)loginValidationWithUser:(NSString*)user andPassword:(NSString*)pass{
    if (user.length == 0 || pass.length == 0) {
        return false;
    }else{
        return true;
    }
}

- (void)displayUIAlertViewWith:(NSString*)title andMessage:(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = title;
    alert.message = message;
    [alert addButtonWithTitle:@"Accept"];
    [alert show];
}

@end
