//
//  AppDataManager.h
//  newmotion
//
//  Created by Adrian Ortuzar on 10/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface AppDataManager : NSObject {

}

@property (nonatomic, retain) NSDictionary *userModel;
@property (nonatomic, strong) NSArray *chargePointsArray;

+ (id)sharedManager;
- (void)loginWithName:(NSString*)username andPassword:(NSString*)password withCompletionHandler:(void(^)(NSDictionary *user, NSError *error))handler;


@end
