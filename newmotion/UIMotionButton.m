//
//  UIMotionButton.m
//  newmotion
//
//  Created by Adrian Ortuzar on 14/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import "UIMotionButton.h"

@implementation UIMotionButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 45.0);
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 20;
        self.layer.borderColor=[UIColor blueColor].CGColor;
        self.layer.borderWidth=2.0f;
        [self setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
