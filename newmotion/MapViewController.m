//
//  MapViewController.m
//  newmotion
//
//  Created by Adrian Ortuzar on 11/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import "MapViewController.h"
#import "AppDataManager.h"



@interface MapViewController ()

@property (nonatomic, strong) NSArray *chargePointsArray;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.chargePointsArray = [[AppDataManager sharedManager] chargePointsArray];
    
    [self drawPointInMapView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawPointInMapView{
    for (int i = 0; i <= [self.chargePointsArray count] - 1; i++){
        NSDictionary *location = self.chargePointsArray[i];
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        
        CLLocationDegrees lat = [[location objectForKey:@"lat"] doubleValue];
        CLLocationDegrees lng = [[location objectForKey:@"lng"] doubleValue];
        
        CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(lat, lng);
        
        point.coordinate = loc;
        [self.mapView addAnnotation:point];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
