//
//  AppDataManager.m
//  newmotion
//
//  Created by Adrian Ortuzar on 10/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import "AppDataManager.h"



@interface AppDataManager ()

@property (nonatomic, strong) NSDictionary *session;
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AppDataManager


+ (id)sharedManager {
    static AppDataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        //set api manager
        NSURL *baseURL = [NSURL URLWithString:@"https://test-api.thenewmotion.com"];
        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        self.sessionManager.requestSerializer = [[AFHTTPRequestSerializer alloc] init];
        [self.sessionManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [self.sessionManager.requestSerializer setValue:@"Basic dGVzdF9jbGllbnRfaWQ6dGVzdF9jbGllbnRfc2VjcmV0" forHTTPHeaderField:@"Authorization"];
        

        //set charge points data
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sample-json-chargepoints" ofType:@"json"];
        NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        NSData* jsonData = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonError;
        self.chargePointsArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONWritingPrettyPrinted error:&jsonError];
    }
    return self;
}

- (void)loginWithName:(NSString*)username andPassword:(NSString*)password withCompletionHandler:(void(^)(NSDictionary *user, NSError *error))handler{
    NSDictionary *param = @{
                            @"grant_type":@"password",
                            @"username":@"programming-assignment@thenewmotion.com",
                            @"password":@"Zea2E5RA"
                            };
    
    [self.sessionManager POST:@"oauth2/access_token" parameters:param success:^(NSURLSessionDataTask *operation, id responseObject) {
        self.session = responseObject;
        [self getUSerWithCompletionHandler:handler];
        NSLog(@"Success: %@", responseObject);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        handler(nil, error);
        NSLog(@"Error: %@", error);
    }];
}

- (void)getUSerWithCompletionHandler:(void(^)(NSDictionary *user, NSError *error))handler{
    
    NSString *auth = [NSString stringWithFormat:@"%@ %@", [self.session objectForKey:@"token_type"], [self.session objectForKey:@"access_token"]];
    [self.sessionManager.requestSerializer setValue:auth forHTTPHeaderField:@"Authorization"];
    
    [self.sessionManager GET:@"v1/me" parameters:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        self.userModel = responseObject;
        NSLog(@"Success: %@", responseObject);
        handler(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        handler(nil, error);
    }];
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
