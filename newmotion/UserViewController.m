//
//  UserViewController.m
//  newmotion
//
//  Created by Adrian Ortuzar on 11/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import "UserViewController.h"
#import "AppDataManager.h"
#import "MapViewController.h"

@interface UserViewController ()

@property (nonatomic, strong) NSDictionary *user;

@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.

    
    self.user = [[AppDataManager sharedManager] userModel];
    
    self.fullNameLabel.text = [NSString stringWithFormat:@"%@ %@", [self.user objectForKey:@"firstName"], [self.user objectForKey:@"lastName"]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mapButtonAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapViewController *MapVC = [storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    [self.navigationController pushViewController:MapVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
