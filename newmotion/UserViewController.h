//
//  UserViewController.h
//  newmotion
//
//  Created by Adrian Ortuzar on 11/04/15.
//  Copyright (c) 2015 Adrian Ortuzar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;

@end
